#
# Be sure to run `pod lib lint KKIMModule.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KKIMModule'
  s.version          = '0.0.10'
  s.summary          = 'A short description of KKIMModule.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/yangpeng0/kkimmodule'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yangpeng' => 'peng.yang@kikitrade.com' }
  s.source           = { :git => 'https://gitlab.com/yangpeng0/kkimmodule.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'
  
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.vendored_libraries = 'KKIMModule/Classes/TUIKit/TUIChat/VoiceConvert/*.a'

#  s.source_files = 'KKIMModule/Classes/**/*'
  s.resources    = 'KKIMModule/Assets/*','KKIMModule/Other/*'
  s.prefix_header_contents =  '#import "define.h"'
  
  s.default_subspec = 'KKIMController','KKChatHistory','KKIMHelper', 'DuomHelper','TUIKit','TUIKit/TUIChat','TUIKit/TUICore'

  s.subspec 'KKIMController' do |spec|
      spec.source_files = 'KKIMModule/Classes/KKIMController/**/*.{h,m}'
  end
  
  s.subspec 'KKChatHistory' do |spec|
      spec.source_files = 'KKIMModule/Classes/KKChatHistory/**/*.{h,m}'
  end
  
  s.subspec 'KKIMHelper' do |spec|
      spec.source_files = 'KKIMModule/Classes/KKIMHelper/**/*.{h,m}'
  end
  
  s.subspec 'DuomHelper' do |spec|
      spec.source_files = 'KKIMModule/Classes/DuomHelper/**/*.{h,m}'
  end
  
  s.subspec 'TUIKit' do |spec|
      spec.subspec 'TUIChat' do |tuichat|
          tuichat.source_files = 'KKIMModule/Classes/TUIKit/TUIChat/**/*.{h,m,mm}'
      end
      spec.subspec 'TUIOfflinePush' do |tuiofflinepush|
                tuiofflinepush.source_files = 'KKIMModule/Classes/TUIKit/TUIOfflinePush/**/*.{h,m,mm}'
      end
      spec.subspec 'TUICore' do |tuicore|
          tuicore.source_files = 'KKIMModule/Classes/TUIKit/TUICore/**/*.{h,m,mm}'
      end
  end

  
  # s.resource_bundles = {
  #   'KKIMModule' => ['KKIMModule/Assets/*.png']
  # }

  s.dependency 'KKBaseModule'
  s.dependency 'TXIMSDK_Plus_iOS'
  s.dependency 'ReactiveObjC'
  s.dependency 'SDWebImage'
 
end
